<?php

use yii\db\schema;
use yii\db\Migration;

class m160520_114720_init_lead_table extends Migration
{
    public function up()
    {
            $this->createTable(
                'lead',
                [
                    'id' => 'pk',
                    'name' => 'string',
                    'birth_date' =>'date',
                    'status' => 'int',
                    'notes' => 'text',

                ],
                    'ENGINE = InnoDB'


                  );
    }

    public function down()
    {
        echo "m160520_114720_init_lead_table cannot be reverted.\n";

        return false;
    }

 
}
